
function makeURI() {
	var secret = document.getElementById("secret").value;
	var image = document.getElementById("image").value;
	var type = document.querySelector('input[name="type"]:checked').value;
	var uri = "otpauth://" + type + "/";

	uri += "?secret=" + secret;

	if (type == "hotp")
		uri += "&counter=0";

	if (image.length > 0)
		uri += "&image=" + encodeURIComponent(image);

	return uri;
}

function onValueChanged() {
  function check(element, valid) {
    var e = document.getElementById(element);
    var v = valid(e.value);
    e.classList.toggle("error", !v);
    return v;
  }

  var err = false;
  err |= !check("secret", (v) => v.match(/^[a-z2-7]{26,}$/i));

  var prv = document.getElementById("preview");
	var img = document.getElementById("image");
	var src = img.value.length > 0 ? img.value : "img/freeotp.svg";

  img.classList.remove("error");
  prv.src = err ? "img/error.svg" : src;

  var uri = makeURI();
	qrcode.clear();
	qrcode.makeCode(uri);
  document.getElementById("urilink").href = uri;
}

function onRandomClicked() {
  var secret = document.getElementById("secret");
	var bytes = new Uint8Array(35);

	window.crypto.getRandomValues(bytes);
	secret.value = base32.encode(bytes);
	onValueChanged();
}