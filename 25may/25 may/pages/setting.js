﻿$(document).ready(function(){
validate();
});

function validate() {
$("#save").click(function(){
var oldpassword = $("#oldpassword").val();
var newpassword = $("#newpassword").val();
var repeatnewpassword = $("#repeatnewpassword").val();
if (oldpassword == '' || newpassword == '' || repeatnewpassword == '') {
alert("Please fill all fields.");
} else if(newpassword != repeatnewpassword) {
alert("Passwords do not match.");
} else {
resetPassword();
}
});
}


function resetPassword() {
var oldpassword = $("#oldpassword").val();
var newpassword = $("#newpassword").val();
$.ajax({
url: 'http://139.59.88.76:4030/api/v1/user/reset-password',
method: "POST",
data: {oldPassword: oldpassword, newPassword: newpassword},
success: function(data){
alert("Success");
}
});
}
